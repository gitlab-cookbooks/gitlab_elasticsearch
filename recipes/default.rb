#
# Cookbook Name:: gitlab_elasticsearch
# Recipe:: default
# License:: MIT
#
# Copyright 2017, GitLab Inc.
#

elasticsearch_user 'elasticsearch'

elasticsearch_install 'elasticsearch' do
  version node['gitlab_elasticsearch']['es_pkg_version']
end

apt_package 'elasticsearch' do
  version node['gitlab_elasticsearch']['es_pkg_version']
  action :lock
end

apt_package node['gitlab_elasticsearch']['java_pkg'] do
  action %i(install lock)
end

elasticsearch_configure 'elasticsearch' do
  configuration(
    'cluster.name' => node['gitlab_elasticsearch']['cluster.name'],
    'node.name' => node['hostname'],
    'node.data' => node['gitlab_elasticsearch']['node.data'],
    'node.ingest' => node['gitlab_elasticsearch']['node.ingest'],
    'node.master' => node['gitlab_elasticsearch']['node.master'],
    'network.host' => node['gitlab_elasticsearch']['network.host'],
    'discovery.zen.ping.unicast.hosts' => node['gitlab_elasticsearch']['discovery.zen.ping.unicast.hosts'],
    'discovery.zen.minimum_master_nodes' => node['gitlab_elasticsearch']['discovery.zen.minimum_master_nodes'],
    'xpack.security.enabled' => node['gitlab_elasticsearch']['xpack.security.enabled']
  )
  jvm_options node['gitlab_elasticsearch']['jvm_options']
end

elasticsearch_plugin 'prometheus_exporter' do
  url node['gitlab_elasticsearch']['prometheus_exporter']['url']
  not_if '/usr/share/elasticsearch/bin/elasticsearch-plugin list | grep prometheus-exporter'
end

elasticsearch_plugin 'x-pack' do
  only_if do node['gitlab_elasticsearch']['plugins']['x-pack'] end
  not_if '/usr/share/elasticsearch/bin/elasticsearch-plugin list | grep x-pack'
end

Chef::Application.fatal!('LVM enabled but not physical_volumes are defined') if node['gitlab_elasticsearch']['lvm'] && node['gitlab_elasticsearch']['physical_volumes'].empty?

lvm_volume_group 'es_vg' do
  only_if do node['gitlab_elasticsearch']['lvm'] end
  physical_volumes node['gitlab_elasticsearch']['physical_volumes']
  wipe_signatures true

  logical_volume 'es_data' do
    size        '100%FREE'
    filesystem  'xfs'
    mount_point location: '/var/lib/elasticsearch', options: 'noatime,nodiratime'
    stripes     node['gitlab_elasticsearch']['physical_volumes'].length
    stripe_size 256
  end
end

directory '/var/lib/elasticsearch' do
  user  'elasticsearch'
  group 'elasticsearch'
  mode  '0755'
end

elasticsearch_service 'elasticsearch'
