# encoding: UTF-8
name             'gitlab_elasticsearch'
maintainer       'Victor Lopez'
maintainer_email 'ops-contact@gitlab.com'
license          'MIT'
description      'Manages ElasticSearch clusters'
version          '0.1.9'

depends 'elasticsearch', '~> 3.2'
depends 'lvm', '~> 4.1'
