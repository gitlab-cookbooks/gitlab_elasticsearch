# encoding: utf-8
# frozen_string_literal: true

# InSpec tests for recipe gitlab_elasticsearch::default
control 'elasticsearch-process-checks' do
  impact 1.0
  title 'General tests for ElasticSearch service'
  desc '
    This control ensures that:
      * openjdk-8-jre package is installed
      * elasticsearch package is installed
      * elasticsearch service in enabled and running'

  # This is not working due to: https://github.com/chef/inspec/issues/2006
  # Both JRE/ES packages are on hold
  # describe package('openjdk-8-jre') do
  #   it { should be_installed }
  # end

  describe service('elasticsearch') do
    before :all do
      sleep 30
    end
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end

  describe port(9200) do
    it { should be_listening }
    its('addresses') do should include '127.0.0.1' end
    its('processes') do should include 'java' end
  end

  describe port(9300) do
    it { should be_listening }
    its('addresses') do should include '127.0.0.1' end
    its('processes') do should include 'java' end
  end
end

control 'elasticsearch-directory-checks' do
  impact 1.0
  title 'elasticsearch dir is present and has correct permissions'

  describe file('/var/lib/elasticsearch') do
    it { should be_directory }
    its('type') do should eq :directory end
    its('owner') do should eq 'elasticsearch' end
    its('group') do should eq 'elasticsearch' end
    its('mode') { should cmp '0755' }
  end
end
