default['gitlab_elasticsearch']['java_pkg'] = 'openjdk-8-jre'
default['gitlab_elasticsearch']['es_pkg_version'] = '5.4.1'

default['gitlab_elasticsearch']['jvm_options'] = []

default['gitlab_elasticsearch']['cluster.name'] = 'es_cluster'
default['gitlab_elasticsearch']['discovery.zen.ping.unicast.hosts'] = ['127.0.0.1', '[::1]']
default['gitlab_elasticsearch']['discovery.zen.minimum_master_nodes'] = 1
default['gitlab_elasticsearch']['network.host'] = '0.0.0.0'

# https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-node.html
default['gitlab_elasticsearch']['node.data'] = true
default['gitlab_elasticsearch']['node.ingest'] = true
default['gitlab_elasticsearch']['node.master'] = true

default['gitlab_elasticsearch']['lvm'] = false
default['gitlab_elasticsearch']['physical_volumes'] = []

default['gitlab_elasticsearch']['prometheus_exporter']['url'] = "https://github.com/vvanholl/elasticsearch-prometheus-exporter/releases/download/#{node['gitlab_elasticsearch']['es_pkg_version']}.0/elasticsearch-prometheus-exporter-#{node['gitlab_elasticsearch']['es_pkg_version']}.0.zip"

default['gitlab_elasticsearch']['plugins']['x-pack'] = true
default['gitlab_elasticsearch']['xpack.security.enabled'] = false
