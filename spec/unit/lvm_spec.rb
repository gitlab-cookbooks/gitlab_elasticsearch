require 'spec_helper'

describe 'gitlab_elasticsearch::default' do
  context 'default execution' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new { |node|
        node.normal['gitlab_elasticsearch']['lvm'] = true
        node.normal['gitlab_elasticsearch']['physical_volumes'] = []
      }.converge(described_recipe)
    end

    it 'fails since it is mis-configured' do
      expect { chef_run }.to raise_error
    end
  end
end
